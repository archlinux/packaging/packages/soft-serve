# Maintainer: George Rawlinson <grawlinson@archlinux.org>
# Maintainer: Orhun Parmaksız <orhun@archlinux.org>
# Contributor: Christian Muehlhaeuser <muesli at gmail dot com>

pkgname=soft-serve
pkgver=0.8.2
pkgrel=1
pkgdesc='A self-hosted Git server for the command line'
arch=('x86_64')
url='https://github.com/charmbracelet/soft-serve'
license=('MIT')
depends=('glibc' 'git')
makedepends=('go')
backup=('etc/soft-serve.conf')
options=('!lto')
source=(
  "git+$url.git#tag=v$pkgver"
  'soft-serve.conf'
  'systemd.service'
  'sysusers.conf'
  'tmpfiles.conf'
)
sha512sums=('cb53c197cd6564134093e935ad315c78f1e907f71befd2cafa6629b3a0fdca8ce2e93e9b2913d75cd6f2c61f54a01714e8358adf6a01a512cb9bff007734717f'
            'dbb0cb5d5e6c20ddb178c4659e52412ddf386b9938e095ecf9135f5e72791b2ec784466b9387c38576acd6319cd67365f772263926fcecae3abe52d82ae073a5'
            '44704f5a55f9fa8376ee278775f7894fc7489a65ec2caea649abff3b19a37ec5aea77758248d1e18bea1e3a894c8f26016e00b0d0fecac99dcefd3fbe4d4d221'
            'e6e91c34aa172c784dfce0b03d464dd48876fbb42e398ed7ef1b0ce4004bb723ed5a9778553d2341718afc908f09d5534c3971734304bac5b07e5650a035d45b'
            'ece4b43029ac22bbac3b2afebab134df5781d92deed5ef24a96d720da3a0f1a033f0cf3a4a5f6ab40d379f5fb4d10730e8873b1ef94943876cdca8f209b89a59')
b2sums=('20d44d5ab4580792e7c98e8be43b2e0d57d61680d8fff882347bd119b3041e5528e17e0af3ca46abef0deb2952bc84c7d2ea8b8d1ebe495f041e494814f5ad74'
        '9cf6dd9d3296bffa2047209e73801328877cefacbf5ce1feaeefbdcffb11ed86531a762299d7d379efe4e6401b135cd8200b506b2f8ac66d032a9d0bddce37e7'
        '8c1f734d414f22efe201dacafe31b8f4a4aea49698c9fa52c567d4398b45c0c8670d0d3da122e2c099524e88c04dca6270b9a81f38f2e95a8c6336a267ccefcd'
        'd167812509d31d7fba1c1d19ff15eda4329190164cc53ba18d7b1da25c1a6293255a3d501335cc0e67572adb173be7da86b0c7fb6970b11429ec7d4409d93286'
        '9866f155a948357582b464e4a5ea79464ed06c4067add77c444378f4bfc7aab57e7afc455e23a51725e1c0d2ceecda087d811dae3f49178e07ae60afc7361b8b')

prepare() {
  cd "$pkgname"

  # create directory for build output
  mkdir build

  # download dependencies
  export GOPATH="${srcdir}"
  go mod download
}

build() {
  cd "$pkgname"

  # set Go flags
  export CGO_CPPFLAGS="${CPPFLAGS}"
  export CGO_CFLAGS="${CFLAGS}"
  export CGO_CXXFLAGS="${CXXFLAGS}"
  export GOPATH="${srcdir}"

  go build -v \
    -buildmode=pie \
    -mod=readonly \
    -modcacherw \
    -ldflags "-compressdwarf=false \
    -linkmode external \
    -extldflags '${LDFLAGS}' \
    -X main.Version=$pkgver \
    -X main.CommitSHA=$(git rev-parse HEAD)" \
    -o build \
    ./cmd/...

  # generate man page
  ./build/soft man > build/soft.1

  # generate shell completion
  for shell in bash fish zsh; do
    ./build/soft completion "$shell" > "build/$shell.completion"
  done
}

package() {
  # systemd integration
  install -vDm644 systemd.service "$pkgdir/usr/lib/systemd/system/$pkgname.service"
  install -vDm644 sysusers.conf "$pkgdir/usr/lib/sysusers.d/$pkgname.conf"
  install -vDm644 tmpfiles.conf "$pkgdir/usr/lib/tmpfiles.d/$pkgname.conf"
  install -vDm644 soft-serve.conf -t "$pkgdir/etc"

  cd "$pkgname"

  # binary
  install -vDm755 -t "$pkgdir/usr/bin" build/soft

  # shell completion
  install -vDm644 build/bash.completion "$pkgdir/usr/share/bash-completion/completions/soft"
  install -vDm644 build/fish.completion "$pkgdir/usr/share/fish/vendor_completions.d/soft.fish"
  install -vDm644 build/zsh.completion "$pkgdir/usr/share/zsh/site-functions/_soft"

  # man page
  install -vDm644 -t "$pkgdir/usr/share/man/man1" build/soft.1

  # license
  install -vDm644 -t "$pkgdir/usr/share/licenses/$pkgname" LICENSE
}

# vim:set ts=2 sw=2 et:
